package cat.martibarbet.democart.controllers;

import cat.martibarbet.democart.exceptions.SessionExpiredException;
import cat.martibarbet.democart.model.Cart;
import cat.martibarbet.democart.model.dto.CartInfo;
import cat.martibarbet.democart.services.CartService;
import cat.martibarbet.democart.services.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;

@Controller
@RequestMapping("cart")
public class CartController {
    private Logger logger = LoggerFactory.getLogger(CartController.class);

    @Autowired private CartService cartService;


    /** To actively dismiss the cart whenever users are done before their cart session expires.
     * Wouldn't throw any exception if the cart had expired yet.
     *
     * @return <b>205, Void</b>
     */
    @DeleteMapping(path = "/delete/{cartId}")
    public ResponseEntity<Void> dismissCart(@PathVariable Integer cartId){

        logger.info("Customer {}'s cart is being dismissed and all of its products will be returned to stock", cartId);
        cartService.dismissCart(cartId);
        return new ResponseEntity<>(HttpStatus.RESET_CONTENT);
    }


    /** Creates a new CartSession and retrieves it to the user.
     *
     * @return <b>201, new Cart</b>
     * @throws SessionExpiredException if a new session-timed Cart could not be retrieved.
     */
    @GetMapping(path = "create")
    public ResponseEntity<Cart> createCart() throws SessionExpiredException {

        logger.info("Requesting a new Cart");
        return new ResponseEntity<>(cartService.createCart(), HttpStatus.CREATED);
    }


    /** Retrieves a Cart by its session ID.
     *
     * @param cartId the requested ID
     * @return <b>200, a CartInfo instance of a Cart</b>
     * @throws SessionExpiredException If the requested ID is not contained in the SHOP keyset
     */
    @GetMapping(path = "/get/{cartId}", produces= {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<CartInfo> getCartInfo(@PathVariable Integer cartId) throws SessionExpiredException{

        logger.info("Customer {} requests his/her cart info", cartId);
        return new ResponseEntity<>(cartService.getCartInfo(cartId) , HttpStatus.OK);
    }


    /** Adds the requested product to the requested cart
     *
     * @param cartId as PathVariable
     * @param productId as QueryParam
     * @return <200, Void>
     * @throws SessionExpiredException Propagates exception originated at an internal call to <i>getCart(ID)</i>
     */
    @PutMapping(path = "/{cartId}/add")
    public ResponseEntity<Void> addProductToCart(@PathVariable("cartId") Integer cartId,
                                                 @RequestParam("productId") Integer productId) throws SessionExpiredException{
        logger.info("Customer {} wants to add product {} to his/her cart", cartId, productId);
        cartService.addProductToCart(cartId, productId);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    /** Removes the requested product from the requested cart
     *
     * @param cartId as PathVariable
     * @param productId as QueryParam
     * @return <200, Void>
     * @throws SessionExpiredException Propagates exception originated at an internal call to <i>getCart(ID)</i>
     */
    @PutMapping(path = "/{cartId}/remove")
    public ResponseEntity<Void> removeProductFromCart(@PathVariable("cartId") Integer cartId,
                                                      @RequestParam("productId") Integer productId) throws SessionExpiredException {
        logger.info("Customer {} wants to remove product {} to his/her cart", cartId, productId);
        cartService.removeProductFromCart(cartId, productId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
