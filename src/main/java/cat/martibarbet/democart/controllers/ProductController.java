package cat.martibarbet.democart.controllers;

import cat.martibarbet.democart.model.entities.Product;
import cat.martibarbet.democart.model.entities.ShopStock;
import cat.martibarbet.democart.services.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Positive;

@Controller
@RequestMapping("products")
public class ProductController {
    private Logger logger = LoggerFactory.getLogger(ProductController.class);

    @Autowired private ProductService productService;


    @GetMapping(path = "{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Product> getProductById(@PathVariable("id") Integer id){
        logger.info("Retrieving product number {}", id);
        return new ResponseEntity<>( productService.getProductById(id), HttpStatus.OK);
    }

    @PostMapping(path = "save",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Product> createProduct(@RequestBody Product product){
        logger.info("Creating a new Product:[{},{},{}]", product.getId(), product.getDescription(), product.getPrice());
        return new ResponseEntity<>(productService.saveProduct(product), HttpStatus.CREATED);
    }

    @PutMapping(path = "{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Product> changeProductPrice(@PathVariable Integer id, @RequestParam @Positive Double price) throws DataIntegrityViolationException {
        logger.info("Changing price of product {} to {}€", id, price);
        return new ResponseEntity<>(productService.setProductPrice(id, price), HttpStatus.CREATED);
    }

    @DeleteMapping(path = "{id}")
    public ResponseEntity<Void> deleteProductById(@PathVariable Integer id){
        logger.info("Deleting product number {}", id);
        productService.deleteProductById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
