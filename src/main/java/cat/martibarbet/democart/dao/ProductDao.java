package cat.martibarbet.democart.dao;

import cat.martibarbet.democart.model.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public interface ProductDao extends JpaRepository<Product, Integer> {

}
