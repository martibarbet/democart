package cat.martibarbet.democart.dao;

import cat.martibarbet.democart.model.entities.ShopStock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductStocksDao extends JpaRepository<ShopStock, Integer> {

}
