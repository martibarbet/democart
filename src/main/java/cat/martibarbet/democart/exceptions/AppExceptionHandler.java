package cat.martibarbet.democart.exceptions;

import cat.martibarbet.democart.model.dto.HandledExceptionInfo;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.NoSuchElementException;

@ControllerAdvice
public class AppExceptionHandler {


    /** Handles Cart-Session related Exceptions.
     * Expectable when seeking inexistent SessionID's due to requests involving dismissed carts.
     *
     * This handled exception carries the HttpStatusCode to be shown to the user.
     *
     * @param se_exc exception thrown by the service layer
     * @param req web request
     * @return an instance of a DTO named 'HandledExceptionInfo'.
     */
    @ExceptionHandler(value= SessionExpiredException.class)
    public ResponseEntity<HandledExceptionInfo> handleSessionExpiredException(SessionExpiredException se_exc, WebRequest req){
        return new ResponseEntity<>(new HandledExceptionInfo().setTimestamp(LocalDateTime.now())
                                                            .setCause(se_exc.getCausedBy())
                                                        .setDescription(se_exc.getDescrMsg()),
                                    new HttpHeaders(),
                                    HttpStatus.valueOf(se_exc.getStatusCode())
        );
    }


    /** Handles Product related Exceptions.
     * Expectable when seeking non-existent products in the product catalog or the stock.
     *
     * @param nse_exc exception thrown by the service layer
     * @param req web request
     * @return an instance of a DTO named 'HandledExceptionInfo'
     */
    @ExceptionHandler(value= NoSuchElementException.class)
    public ResponseEntity<HandledExceptionInfo> handleNoSuchElementException(NoSuchElementException nse_exc, WebRequest req){
        return new ResponseEntity<>(new HandledExceptionInfo().setTimestamp(LocalDateTime.now())
                                                            .setCause("Product Unavailable")
                                                        .setDescription("No such product exists in our catalog or we haven't stocked it up yet."),
                                    new HttpHeaders(),
                                    HttpStatus.NOT_FOUND
        );
    }


    /** Handles Transaction related Exceptions.
     * Expectable when picking amount of an empty stock.
     *
     * @param ts_Exc exception thrown by the service layer
     * @param req web request
     * @return an instance of a DTO named 'HandledExceptionInfo'
     */
    @ExceptionHandler(value= TransactionSystemException.class)
    public ResponseEntity<HandledExceptionInfo> handleTransactionSystemException(TransactionSystemException ts_Exc, WebRequest req){
        return new ResponseEntity<>(new HandledExceptionInfo().setTimestamp(LocalDateTime.now())
                                                            .setCause("No remaining stock of this product.")
                                                        .setDescription("Sold Out! Seems we ran out of this product's stock. Try again later!"),
                                    new HttpHeaders(),
                                    HttpStatus.CONFLICT
        );
    }

    /** Handles Cosntraint exceptions
     *
     * @param exc exception thrown by service layer
     * @param req the web request
     * @return an instance of a DTO named 'HandledExceptionInfo'
     */
    @ExceptionHandler(value = ConstraintViolationException.class)
    public ResponseEntity<HandledExceptionInfo> handleExceptionInsufficientStock(ConstraintViolationException exc, WebRequest req){
        return new ResponseEntity<>(
                new HandledExceptionInfo().setTimestamp(LocalDateTime.now())
                        .setCause("CANNOT SET DB STATE TO WHAT YOU REQUESTED")
                        .setDescription(exc.getMessage())
                        .setErrors(new HashMap<>(){{ exc.getConstraintViolations().forEach(
                                constraint ->   put(constraint.getConstraintDescriptor().getMessageTemplate(),
                                        constraint.getInvalidValue().toString()));}}),
                new HttpHeaders(),
                HttpStatus.NOT_ACCEPTABLE
        );
    }
}
