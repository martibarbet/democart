package cat.martibarbet.democart.exceptions;

/**
 *  Custom Exception to avoid having NullPointers when accessing SHOP_CARTS by ID
 *
 *  Ideally, to be thrown only by Bad User Requests.
 */
public class SessionExpiredException extends Exception {

    /** HttpStatus Code to be returned*/
    private Integer statusCode;
    /** User friendly problem cause*/
    private String causedBy;
    /** Further problem description */
    private String descrMsg;

    public SessionExpiredException(String causedBy, String _descr, Integer statusCode) {
        this.causedBy = causedBy;
        this.descrMsg = _descr;
        this.statusCode = statusCode;
    }

    public String getDescrMsg() {
        return descrMsg;
    }
    public void setDescrMsg(String descrMsg) {
        this.descrMsg = descrMsg;
    }

    public String getCausedBy() {
        return causedBy;
    }
    public void setCausedBy(String causedBy) {
        this.causedBy = causedBy;
    }

    public Integer getStatusCode() {
        return statusCode;
    }
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }
}
