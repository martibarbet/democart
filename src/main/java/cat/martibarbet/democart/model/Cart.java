package cat.martibarbet.democart.model;

import cat.martibarbet.democart.model.entities.Product;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Cart {

    private Integer sessionId;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime sessionExpiration;

    private Map<Integer,List<Product>> products;



    public Cart(Integer sessionId, Long expireIn, String expireUnit) {
        this.sessionId = sessionId;
        this.sessionExpiration = LocalDateTime.now().plus(expireIn, ChronoUnit.valueOf(expireUnit));
        this.products = new HashMap<>(0);
    }


    public Integer getSessionId() {
        return sessionId;
    }
    public void setSessionId(Integer sessionId) {
        this.sessionId = sessionId;
    }

    public LocalDateTime getSessionExpiration() {
        return sessionExpiration;
    }
    public void setSessionExpiration(LocalDateTime sessionExpiration) {
        this.sessionExpiration = sessionExpiration;
    }

    public Map<Integer,List<Product>> getProducts() {
        return products;
    }
    public void setProducts(Map<Integer, List<Product>> products) {
        this.products = products;
    }
}
