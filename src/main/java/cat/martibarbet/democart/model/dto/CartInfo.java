package cat.martibarbet.democart.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * DTO POJO to Show a Cart to users
 */
public class CartInfo {

    private Integer sessionId;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime sessionExpiration;

    private Map<Integer, List<ProductInfo>> products;


    public CartInfo(Integer sessionId) {
        this.sessionId = sessionId;
        this.products = new HashMap<>(0);
    }

    public Map<Integer,List<ProductInfo>> getProducts() {
        return products;
    }
    public void setProducts(Map<Integer, List<ProductInfo>> products) {
        this.products = products;
    }

    public Integer getSessionId() {
        return sessionId;
    }
    public void setSessionId(Integer sessionId) {
        this.sessionId = sessionId;
    }

    public LocalDateTime getSessionExpiration() {
        return sessionExpiration;
    }
    public void setSessionExpiration(LocalDateTime sessionExpiration) {
        this.sessionExpiration = sessionExpiration;
    }
}
