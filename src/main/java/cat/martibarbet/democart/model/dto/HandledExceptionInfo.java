package cat.martibarbet.democart.model.dto;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * DTO POJO to show users handled exceptions in an comprehensible manner.
 * Instantiation by setters instead of ConstructorParams to make it easier to read.
 */
public class HandledExceptionInfo {

    private LocalDateTime timestamp;
    private String cause;
    private String description;
    private Map<String, String> errors;


    public HandledExceptionInfo() {}


    public LocalDateTime getTimestamp() {
        return timestamp;
    }
    public HandledExceptionInfo setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public String getDescription() {
        return description;
    }
    public HandledExceptionInfo setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getCause() {
        return cause;
    }
    public HandledExceptionInfo setCause(String cause) {
        this.cause = cause;
        return this;
    }

    public Map<String, String> getErrors() {
        return errors;
    }
    public HandledExceptionInfo setErrors(Map<String, String> errors) {
        this.errors = errors;
        return this;
    }
}
