package cat.martibarbet.democart.model.dto;

/**
 * DTO POJO to Show a Product to users
 */
public class ProductInfo {
    private String description;
    private Double price;

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }
    public void setPrice(Double price) {
        this.price = price;
    }
}
