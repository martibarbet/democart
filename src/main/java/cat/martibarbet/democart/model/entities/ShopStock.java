package cat.martibarbet.democart.model.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.io.Serializable;

@Entity
@Table(name= "shop_stock")
public class ShopStock implements Serializable {

    @Id
    @Column(name="product_id")
    private Integer productId;

    @Column(name = "amount")
    @Min(value = 0, message = "We ran out of stock. You may try to order less items, if possible.")
    private Integer amount;

    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    @JsonBackReference
    private Product product;

    public ShopStock(){}


    public Integer getProductId() {
        return productId;
    }
    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getAmount() {
        return amount;
    }
    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Product getProduct() {
        return product;
    }
    public void setProduct(Product product) {
        this.product = product;
    }

}
