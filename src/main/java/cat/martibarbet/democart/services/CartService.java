package cat.martibarbet.democart.services;

import cat.martibarbet.democart.exceptions.SessionExpiredException;
import cat.martibarbet.democart.model.Cart;
import cat.martibarbet.democart.model.dto.CartInfo;

public interface CartService {

    /** Erases a Cart from memory.
     * Might be called by user action but most often will be used by the session expiry interface.
     * @param id the session/cart ID.
     */
    void dismissCart(Integer id);   //throwing exceptions is needless here


    /** Creates and returns a timed Cart instance.
     * Calls cartExpiryService for the purposes of getting a Cart's Session_ID assigned & Timer triggered.
     * @return a new Cart with it's session timer
     */
    Cart createCart()  throws SessionExpiredException;


    /** Gets an active cart from memory by a given session ID.
     * Is only called internally. Never by users.
     *
     * @param id session/cart ID
     * @return a cart from SHOP
     * @throws SessionExpiredException If the requested ID is not in the SHOP keyset
     */
    Cart getCart(Integer id) throws SessionExpiredException;


    /** Retrieves a Cart by its ID
     *
     * @param id asked by the user
     * @return the requested Cart mapped to a CartInfo DTO.
     * @throws SessionExpiredException Propagates exception originated at a call to <i>getCart(ID)</i>
     */
    CartInfo getCartInfo(Integer id) throws SessionExpiredException;


    /** Adds to a CartID one item of a product
     *
     * @param cartId ID of the cart to which put a product
     * @param productId ID of the product to be added to the cart
     * @return whether if this product could be added to this cart.
     * @throws SessionExpiredException Propagates exception originated at a call to <i>getCart(ID)</i>
     */
    boolean addProductToCart(Integer cartId, Integer productId) throws SessionExpiredException;


    /** Removes from a CartID one item of a product
     *
     * @param cartId ID of the cart to which remove the product
     * @param productId ID of the product to be removed from the cart
     * @throws SessionExpiredException Propagates exception originated at a call to <i>getCart(ID)</i>
     */
    void removeProductFromCart(Integer cartId, Integer productId) throws SessionExpiredException;
}
