package cat.martibarbet.democart.services;

import cat.martibarbet.democart.model.entities.Product;
import cat.martibarbet.democart.model.entities.ShopStock;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.NoSuchElementException;
import java.util.Optional;

public interface ProductService {

    //CRUD OPS
    Product saveProduct(Product product);
    Product getProductById(Integer id);
    void deleteProductById(Integer id);
    Product setProductPrice(Integer productId, Double price);

    //SHOPING OPS
    Product pickProductFromStock(Integer id);
    void putProductToStock(Product fromCart);
}
