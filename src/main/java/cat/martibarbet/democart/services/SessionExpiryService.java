package cat.martibarbet.democart.services;

import cat.martibarbet.democart.model.Cart;

public interface SessionExpiryService {

    /** Sets the TimerTask delay in Millis.
     *  The unit and value are picked from application.properties through @Value injected properties
     *  Converts the unit duration to millis and multiplies it by value.
     *  Units must be in Enum java.time.temporal.ChronoUnit
     * @return delay in miliseconds
     */
    Long getExpirationTimeInMillis();


    /** Instantiator of timed Carts.
     *  Kicks a TimerTask Thread that when the delay is a @Value property-
     *  Upon delay expiry a call to the cartService dismiss method is made.
     *
     * @return The Cart after setting its Thread Timer.
     */
    Cart getTimedCartInstance();

}
