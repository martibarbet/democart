package cat.martibarbet.democart.services.impl;

import cat.martibarbet.democart.exceptions.SessionExpiredException;
import cat.martibarbet.democart.model.Cart;
import cat.martibarbet.democart.model.entities.Product;
import cat.martibarbet.democart.model.dto.CartInfo;
import cat.martibarbet.democart.model.dto.ProductInfo;
import cat.martibarbet.democart.services.SessionExpiryService;
import cat.martibarbet.democart.services.CartService;
import cat.martibarbet.democart.services.ProductService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;


import javax.validation.constraints.Null;
import java.util.*;
import java.util.stream.Collectors;

@Service
//@Transactional
@PropertySource("classpath:application.properties")
public class CartServiceImpl implements CartService {

    @Autowired private ProductService productService;
    @Autowired private SessionExpiryService expiryService;
    @Autowired private ModelMapper mapper;

    /**
     * THE IN-MEMORY SESSION SYSTEM.
     */
    private final static Map<Integer,Cart> SHOP_CARTS = new HashMap<>();


    @Override
    public Cart createCart() throws SessionExpiredException {
        Cart newCart = expiryService.getTimedCartInstance();
        SHOP_CARTS.putIfAbsent(newCart.getSessionId(), newCart);
        try {
            return getCart(newCart.getSessionId());
        }catch (SessionExpiredException | NullPointerException  ses){
            throw new SessionExpiredException(null, "We're sorry we couldn't create your cart. Try again.", 500);
        }
    }


    @Override
    public Cart getCart(Integer id) throws SessionExpiredException {
        if (SHOP_CARTS.containsKey(id)) {
            return SHOP_CARTS.get(id);
        }
        throw new SessionExpiredException("Your session expired", "You cart has been dismissed and no longer exists.", 410);
    }


    @Override
    public CartInfo getCartInfo(Integer id) throws SessionExpiredException {

        CartInfo cartInfo = new CartInfo(id);

        getCart(id).getProducts().forEach((key, value) -> {
            List<ProductInfo> dto = value.stream().map(entity->mapper.map(entity, ProductInfo.class)).collect(Collectors.toList());
            cartInfo.getProducts().put(key, dto);
        });

        cartInfo.setSessionExpiration(getCart(id).getSessionExpiration());

        return cartInfo;
    }


    @Override
    public boolean addProductToCart(Integer cartId, Integer productId) throws SessionExpiredException {

        Product product = productService.pickProductFromStock(productId);
        if (getCart(cartId).getProducts().containsKey(product.getId())) {
            return getCart(cartId).getProducts().get(product.getId())
                   .add(productService.saveProduct(product));
        } else {
            List<Product> itemsOfProduct = new ArrayList<>();
            itemsOfProduct.add(product);
            getCart(cartId).getProducts().put(productService.saveProduct(product).getId(), itemsOfProduct);
            return true;
        }
    }


    @Override
    public void removeProductFromCart(Integer cartId, Integer productId) throws SessionExpiredException {
        if(getCart(cartId).getProducts().containsKey(productId)){
            productService.putProductToStock(productService.getProductById(productId));
            getCart(cartId).getProducts().get(productId).remove(0);

            if(getCart(cartId).getProducts().get(productId).isEmpty()){
                getCart(cartId).getProducts().keySet().remove(productId);
            }
        }
    }


    @Override
    public void dismissCart(Integer id){
        try {
            Cart cart = getCart(id);
            cart.getProducts().forEach((key, value) -> value.forEach(
                    productItem -> productService.putProductToStock(productItem)
            ));
            SHOP_CARTS.remove(id);
        }catch (SessionExpiredException ignored){}//Not even worth propagating after we couldn't find a cart whoose owner wants to dismiss anyway. He doesn't give a shit, neither we do.
    }

}
