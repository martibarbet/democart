package cat.martibarbet.democart.services.impl;

import cat.martibarbet.democart.dao.ProductDao;
import cat.martibarbet.democart.dao.ProductStocksDao;
import cat.martibarbet.democart.model.entities.Product;
import cat.martibarbet.democart.model.entities.ShopStock;
import cat.martibarbet.democart.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired private ProductDao productDao;
    @Autowired private ProductStocksDao stocksDao;


    public Product saveProduct(Product product){
        stocksDao.save((product.getShopStock()));
        return productDao.save(product);
    }


    public Product getProductById(Integer id) {
        return productDao.findById(id).orElseThrow();
    }


    public void deleteProductById(Integer id){
        productDao.deleteById(id);
    }


    public Product setProductPrice(Integer productId, Double price) {
        Product product = productDao.findById(productId).orElseThrow();
        product.setPrice(price);
        return productDao.save(product);
    }


    public Product pickProductFromStock(Integer id){
        Product product = productDao.findById(id).orElseThrow();
        product.getShopStock().setAmount(
                stocksDao.findById(id).orElseThrow().getAmount() -1
        );
        return product;
    }



    public void putProductToStock(Product fromCart) {
        ShopStock productStock = stocksDao.findById(fromCart.getId()).orElseThrow();
        productStock.setAmount( productStock.getAmount() +1 );
        stocksDao.save(productStock);
    }
}
