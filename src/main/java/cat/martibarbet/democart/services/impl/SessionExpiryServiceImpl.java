package cat.martibarbet.democart.services.impl;

import cat.martibarbet.democart.model.Cart;
import cat.martibarbet.democart.services.SessionExpiryService;
import cat.martibarbet.democart.services.CartService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Timer;
import java.util.TimerTask;

@Service
@PropertySource("classpath:application.properties")
public class SessionExpiryServiceImpl implements SessionExpiryService {

    private Logger logger = LoggerFactory.getLogger(SessionExpiryServiceImpl.class);

    @Autowired private CartService cartService;

    /**
     * Length value of a session timer */
    @Value("${appprop.cart.exp.time}")
    private Long expirationValue;

    /**
    * Length Unit of a session timer */
    @Value("${appprop.cart.exp.unit}")
    private String expirationUnit;

    /** Static Session Identifier */
    private static Integer SESSION_ID = 0;



    public Cart getTimedCartInstance() {
        Integer sessionId = ++SESSION_ID;
        TimerTask sessionTask = new TimerTask() {
            @Override
            public void run() {
                cartService.dismissCart(sessionId);
                logger.info("       U S E R  #"+sessionId+"   FINISHED IT'S SESSION   |)>>>>--- ");
            }
        };
        Timer timer = new Timer("Customer #"+sessionId);
        timer.schedule(sessionTask, getExpirationTimeInMillis());

        logger.info("|)>>>>---   U S E R  #"+sessionId+"   BEGAN IT'S SESSION    ");
        return new Cart(sessionId, expirationValue, expirationUnit);
    }


    public Long getExpirationTimeInMillis(){
        Duration duration = ChronoUnit.valueOf(expirationUnit).getDuration();
        Long unitDuration = duration.toMillis();
        return unitDuration * expirationValue;
    }
}
