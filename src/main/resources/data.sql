DROP TABLE IF EXISTS PRODUCTS;
DROP TABLE IF EXISTS SHOP_STOCK;

create table products(
    id          INTEGER     NOT NULL,
    description VARCHAR(25) NOT NULL,
    price       DOUBLE      NOT NULL,
    PRIMARY KEY (id)
);

create table shop_stock(
    product_id  INTEGER     PRIMARY KEY,
    amount      INTEGER     NOT NULL,
    FOREIGN KEY (product_id) REFERENCES products (id)
);

insert into products (id, description, price) values(1, 'Liquid Hand soap', 3.5);
insert into products (id, description, price) values(2, 'Comb', 11.25);
insert into products (id, description, price) values(3, 'Brush', 17.0);
insert into products (id, description, price) values(4, 'Shampoo', 9.55);
insert into products (id, description, price) values(5, 'Lipstick', 10.40);
insert into products (id, description, price) values(6, 'Eyeliner', 24.99);
insert into products (id, description, price) values(7, 'Antiaging mask', 45.00);

insert into shop_stock (product_id, amount) values(1, 2);
insert into shop_stock (product_id, amount) values(2, 1);
insert into shop_stock (product_id, amount) values(3, 1);
insert into shop_stock (product_id, amount) values(4, 1);
insert into shop_stock (product_id, amount) values(5, 1);
insert into shop_stock (product_id, amount) values(6, 1);
insert into shop_stock (product_id, amount) values(7, 0);