package cat.martibarbet.democart.services.impl;

import cat.martibarbet.democart.dao.ProductStocksDao;
import cat.martibarbet.democart.exceptions.SessionExpiredException;
import cat.martibarbet.democart.model.Cart;
import cat.martibarbet.democart.model.dto.CartInfo;
import cat.martibarbet.democart.services.CartService;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.TransactionSystemException;

import java.util.NoSuchElementException;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CartServiceImplTest {

    @Autowired private CartService cartService;
    @Autowired private ProductStocksDao stocksDao;

    @BeforeAll
    void resetContext(){

    }

    @Test
    @Order(1)
    void createCart() throws SessionExpiredException {
        Cart cart = cartService.createCart();
        Assertions.assertEquals(2, cart.getSessionId());
    }//SHOP has Cart#1

    @Test
    @Order(2)
    void addProductToCart() throws SessionExpiredException {
        cartService.addProductToCart(2, 1);
        Assertions.assertFalse(cartService.getCart(2).getProducts().isEmpty());
    }//SHOP has Cart#1 with 1 unit of Product1

    @Test
    @Order(3)
    void addProductToCart_whenProductDoesNotExist() throws SessionExpiredException {
        Assertions.assertThrows(NoSuchElementException.class, ()->cartService.addProductToCart(2, 8));
    }//SHOP stays the same

    @Test
    @Order(4)
    void addProductToCart_whenProductSoldOut(){
        Assertions.assertThrows(TransactionSystemException.class, ()->cartService.addProductToCart(2, 7));
    }//SHOP stays the same


    @Test
    @Order(5)
    void removeProductsFromCart() throws SessionExpiredException {
        cartService.removeProductFromCart(2, 1);
        Cart cart = cartService.getCart(2);
        Assertions.assertTrue(cart.getProducts().isEmpty());
    }//SHOP has Cart#1 with no products

    @Test
    @Order(6)
    void getCart() throws SessionExpiredException {
        Assertions.assertNotNull(cartService.getCart(2));
    }// SHOP can retrieve Cart By id 1

    @Test
    @Order(7)
    void getCartInfoMapsCorrectlyToDTO() throws SessionExpiredException {
        //Put Product to Cart:
        cartService.addProductToCart(2,5);

        //When CartInfo is requested both the cart and the product info are mapped to DTO
        CartInfo info = cartService.getCartInfo(2);
        Assertions.assertTrue(info != null
                                    && info.getProducts().get(5).get(0) != null
                                    && info.getSessionId()!= null
                                    && info.getSessionExpiration()!=null
                                    && info.getProducts().get(5).get(0).getDescription() != null
                                    && info.getProducts().get(5).get(0).getPrice() != null
        );
    }// SHOP stays the same

    @Test
    @Order(8)
    void dismissCart() throws SessionExpiredException {
        cartService.dismissCart(2);
        Assertions.assertThrows(SessionExpiredException.class, ()-> cartService.getCart(1));
    }// SHOP looses Cart#1 and Stock gains 1 unit of Product5

    @Test
    @Order(9)
    void dismissingCart_returnsProductsToStock(){
        Assertions.assertEquals(1, (int) stocksDao.findById(5).get().getAmount());
    }
}