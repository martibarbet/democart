package cat.martibarbet.democart.services.impl;

import cat.martibarbet.democart.model.entities.Product;
import cat.martibarbet.democart.model.entities.ShopStock;
import cat.martibarbet.democart.services.ProductService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import java.util.NoSuchElementException;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
class ProductServiceImplTest {

    @Autowired private ProductService productService;


    @Test
    void saveProduct() {
        Product sample = new Product();
                sample.setId(8);
                sample.setDescription("Sample");
                sample.setPrice(10.0);
        ShopStock stock = new ShopStock();
                stock.setAmount(4);

        sample.setShopStock(stock);
        stock.setProduct(sample);

        Assertions.assertNotNull(productService.saveProduct(sample));
    }

    @Test
    void getProductById() {
        Product comb = productService.getProductById(2);
        Assertions.assertTrue(
                comb.getId().equals(2)  &&  comb.getDescription().equals("Comb")  &&  comb.getPrice().equals(11.25)
        );
    }

    @Test
    void getProductById_failsIfInexistent() {
        // Product 12 is not in DB
        Assertions.assertThrows(NoSuchElementException.class, ()->{productService.getProductById(12);});
    }

    @Test()
    void deleteProductById(){
        Assertions.assertDoesNotThrow(()->{productService.deleteProductById(1);});
    }

    @Test
    void setProductPrice() {
        Product p6NewPrice = productService.getProductById(6);
        p6NewPrice.setPrice(16.5);
        Assertions.assertEquals(p6NewPrice, productService.setProductPrice(6, 16.5));
    }

    @Test
    void pickProductFromStock_whenStockAvailableSucceeds() {
        Product comb = productService.pickProductFromStock(2);
        Assertions.assertTrue(
                comb.getId().equals(2)  &&  comb.getDescription().equals("Comb")  &&  comb.getPrice().equals(11.25) && comb.getShopStock().getAmount()== 0
        );
    }

    @Test
    void pickProductFromStock_whenNoShopStockExists() {
        // Stock of product 11 is not present in DB
        Assertions.assertThrows(NoSuchElementException.class, ()->{productService.pickProductFromStock(11);});
    }

    @Test
    void letProductBackToStock() {
        // There's one product 3 in stock
        Product brush = new Product();
                brush.setId(3);
                brush.setDescription("Brush");
                brush.setPrice(17.0);
        // Fake returning it should set brush stocks to two
        productService.putProductToStock(brush);
        Assertions.assertEquals(2, productService.getProductById(3).getShopStock().getAmount());
    }
}