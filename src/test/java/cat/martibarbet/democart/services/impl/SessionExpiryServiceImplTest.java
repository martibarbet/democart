package cat.martibarbet.democart.services.impl;

import cat.martibarbet.democart.model.Cart;
import cat.martibarbet.democart.services.CartService;
import cat.martibarbet.democart.services.SessionExpiryService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class SessionExpiryServiceImplTest {

   @Autowired private SessionExpiryService sessionService;


    @Test
    void getExpirationTimeInMillis(){
        Long millis = sessionService.getExpirationTimeInMillis();
        Assertions.assertEquals(600000L, millis);
    }

    @Test
    void getTimedCartInstance(){
        LocalDateTime interval1 = LocalDateTime.now().plus(sessionService.getExpirationTimeInMillis(), ChronoUnit.MILLIS);
            Cart cart = sessionService.getTimedCartInstance();
        LocalDateTime interval2 = LocalDateTime.now().plus(sessionService.getExpirationTimeInMillis(), ChronoUnit.MILLIS);

        boolean expiration = ! interval1.isAfter(cart.getSessionExpiration())
                            && !interval2.isBefore(cart.getSessionExpiration());
        boolean session = cart.getSessionId() == 1;
        boolean productMap = cart.getProducts().size()==0;

        Assertions.assertTrue(expiration && session && productMap);
    }

}
